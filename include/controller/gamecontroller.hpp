#pragma once

#include <GLFW/glfw3.h>
#include <core/snakegame.hpp>
#include <memory>
#include <GameView.h>
#include <boost/timer.hpp>

class GameController {
public:
    static GameController* instance(SnakeGame* game, GLFWwindow* window, GameView* view);

	void start();
    void render();
    void update();
private:
    GameController(SnakeGame* game, GLFWwindow* window, GameView* view);
    static GameController* m_instance;
    std::auto_ptr<SnakeGame> m_game;
    GLFWwindow* m_window;
    std::auto_ptr<GameView> m_view;

    static void keyboardHandler(GLFWwindow* window, int key, int scancode,
                         int action, int modifiers);

    static void mousePositionHandler(GLFWwindow* window, double x, double y);

    std::auto_ptr<boost::timer> m_timer;

    double m_stepInterval;
};
