#pragma once
#include "cellobject.hpp"
#include <vector>
#include <memory>

enum Direction
{
    Up, Right, Down, Left
};

struct Square
{
    Square(size_t w, size_t h);

    std::shared_ptr<CellObject> cell(int x, int y) const;
    std::shared_ptr<CellObject> next(std::shared_ptr<CellObject> cell, Direction dir) const;

private:
    int mx[4] = {0, 1, 0, -1};
    int my[4] = {1, 0, -1, 0};

    size_t m_w;
    size_t m_h;
    std::vector< std::vector<std::shared_ptr<CellObject> > > m_field;

};
