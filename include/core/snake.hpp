#pragma once

#include <core/cellobject.hpp>
#include <core/square.hpp>
#include <vector>
#include <list>


class Snake
{
public:
    Snake(std::shared_ptr<CellObject> init);
    void move_head(std::shared_ptr<CellObject> head);
    void move_tail();
    std::shared_ptr<CellObject> head();
    Direction direction();
    void turn(Direction direction);

private:
    Direction m_direction;
    std::list<std::shared_ptr<CellObject>> m_cells;
    CellObject::SnakeSegmentType m_last_turn;
};


