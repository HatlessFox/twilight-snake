#pragma once

#include <cstddef>

/**
 * @brief Cell object
 */
class CellObject
{
public:
    enum Type
    {
        Empty,
        Food,
        Snake
    };
    
    enum SnakeSegmentType
    {
    	Unknown,
    	Vertical,
    	Horizontal,
    	LeftToUp,
    	LeftToDown,
    	RightToUp,
    	RightToDown
    };

    explicit CellObject(int x, int y, Type type = Empty);

    int x() const;

    int y() const;

    Type type() const;
    
    SnakeSegmentType snake_segment_type() const;
    
    void set_type(CellObject::Type type);
    
    void set_snake_segment_type(CellObject::SnakeSegmentType type);

protected:
private:

    int m_x;
    int m_y;
    Type m_type;
    SnakeSegmentType m_snake_segment_type;
};
