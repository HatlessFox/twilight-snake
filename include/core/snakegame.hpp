#pragma once

#include "cellobject.hpp"
#include "snake.hpp"
#include "square.hpp"
#include <cstddef>
#include <vector>
#include <list>
#include <memory>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/random.hpp>

/**
 * @brief The SnakeGame class
 */
class SnakeGame
{
public:
    /**
     * @brief SnakeGame constructor
     * @param w width of field
     * @param h height of field
     */
    SnakeGame(size_t w, size_t h);

    /**
     * @brief Resets current game to initial state
     */
    void reset();

    /**
     * @brief Tells whether current game is over
     * @return
     */
    bool isGameOver();

    /**
     * @brief Makes a step
     */
    void step();

    /**
     * @brief Snake should go in given direction on next step
     */
    void turn(Direction direction);

    std::shared_ptr<Square> field() {
        return m_square;
    }

    std::shared_ptr<Snake> snake() {
        return m_snake;
    }

    size_t width() const {
        return m_w;
    }

    size_t height() const {
        return m_h;
    }

private:
    size_t m_w;
    size_t m_h;
    std::shared_ptr<Square> m_square;
    std::shared_ptr<Snake> m_snake;
    bool m_is_game_over;
    CellObject::SnakeSegmentType m_last_turn;
    boost::random::mt19937 m_generator;
    boost::random::uniform_int_distribution<> m_distw;
    boost::random::uniform_int_distribution<> m_disth;


    void generateFood();
};
