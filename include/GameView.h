#ifndef __GAME_VIEW_H__
#define __GAME_VIEW_H__

enum CellType {
  EMPTY,
  FOOD,
  SNAKE_HORIZ,
  SNAKE_VERT,
  SNAKE_LOW_LEFT,
  SNAKE_LOW_RIGHT,
  SNAKE_UP_LEFT,
  SNAKE_UP_RIGHT
};

class GameView {
public:
  virtual void begin() = 0;  // call before render session
  virtual void drawItem(unsigned hCell, unsigned vCell, CellType type) = 0;
  virtual void commit() = 0; // call when render session is done
};


#endif 
