#include <controller/gamecontroller.hpp>
#include <iostream>
#include <boost/shared_ptr.hpp>
#include <boost/thread/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>

GameController* GameController::m_instance = nullptr;

GameController::GameController(SnakeGame* game, GLFWwindow *window, GameView *view)
    :  m_game(game), m_window(window), m_view(view), m_timer(new boost::timer),
       m_stepInterval(.04)
{   }

GameController* GameController::instance(SnakeGame* game, GLFWwindow *window, GameView *view) {
    if (m_instance == nullptr) {
        m_instance = new GameController(game, window, view);
        glfwSetKeyCallback(window, GameController::keyboardHandler);
        glfwSetCursorPosCallback(window, GameController::mousePositionHandler);
    }
    return m_instance;
}

void GameController::render() {
    for (size_t i = 0; i < m_game->width(); ++i) {
        for (size_t j = 0; j < m_game->height(); ++j) {
            std::shared_ptr<CellObject> obj = m_game->field()->cell(i, j);
            switch (obj->type()) {
            case CellObject::Empty:
                m_view->drawItem(i, j, EMPTY);
                break;
            case CellObject::Food:
                m_view->drawItem(i, j, FOOD);
                break;
            case CellObject::Snake:
                switch (obj->snake_segment_type()) {
                case CellObject::Horizontal:
                    m_view->drawItem(i, j, SNAKE_HORIZ);
                    break;
                case CellObject::Vertical:
                    m_view->drawItem(i, j, SNAKE_VERT);
                    break;
                case CellObject::LeftToDown:
                    break;
                case CellObject::LeftToUp:
                    break;
                case CellObject::RightToDown:
                    break;
                case CellObject::RightToUp:
                    break;
                default:
                    ;
                }
                break;
            }
        }
    }

}

void GameController::start() {
    m_timer->restart();
}

void GameController::update() {
    if (m_timer->elapsed() >= m_stepInterval) {
        m_game->step();
        m_timer->restart();
    }
}

void GameController::keyboardHandler(GLFWwindow *window, int key, int scancode,
                                     int action, int modifiers)
{
    if (m_instance == nullptr)
        return;

    //XXX: directions are in a mess
    switch (key) {
    case GLFW_KEY_UP:
        m_instance->m_game->turn(Left);
        break;
    case GLFW_KEY_DOWN:
        m_instance->m_game->turn(Right);
        break;
    case GLFW_KEY_LEFT:
        m_instance->m_game->turn(Up);
        break;
    case GLFW_KEY_RIGHT:
        m_instance->m_game->turn(Down);
        break;
    }
}

void GameController::mousePositionHandler(GLFWwindow *window, double x, double y) {
    //not used
}
