#include <memory>
#include <unordered_map>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/system/error_code.hpp>

using namespace boost::asio;

io_service service;

#define MEM_FN(x) boost::bind(&SelfType::x, shared_from_this())
#define MEM_FN1(x,y) boost::bind(&SelfType::x, shared_from_this(),y)
#define MEM_FN2(x,y,z) boost::bind(&SelfType::x, shared_from_this(),y,z)

class Client : public boost::enable_shared_from_this<Client>, boost::noncopyable {
public:
    typedef Client SelfType;
    typedef boost::system::error_code ErrorCode;
    typedef std::shared_ptr<Client> ptr;

    bool isClientStarted() {
        return isStarted;
    }

    void stop() {
        if (!isStarted) {
            return;
        }
        std::cout << "Stop " + username << std::endl;
        mySocket.close();
    }

    static ptr start(ip::tcp::endpoint ep, std::string username) {
        ptr client(new Client(username));
        client -> start(ep);
        return client;
    }

private:

    Client(const std::string& username) : mySocket(service), isStarted(true), username(username), timer(service),
    handlers({
        {"wait", &Client::onWait}
//        {"ready", MEM_FN1(onReady, _1)},
//        {"move", MEM_FN1(onMove, _1)},
//        {"start", MEM_FN1(onStart, _1)},
//        {"current", MEM_FN1(onWait, _1)}
    }) {
    }

    void onConnect(const ErrorCode& err) {
        if (!err) {
            doWrite("login " + username + '\n');
        } else {
            stop();
        }
    }

    void onWrite(const ErrorCode& ec, size_t bytes) {
        doRead();
    }

    void onRead(const ErrorCode& err, size_t bytes) {
        if (err) {
            stop();
        }
        if (!isStarted) {
            return;
        }
        std::string msg(readBuffer, bytes);
        auto f = std::find(msg.begin(), msg.end(), ' ');
        std::string key(msg.begin(), f);
        std::string value(f + 1, msg.end());
        auto handler = handlers.find(key);
        if (handler != handlers.end()) {
            (this->*(handler -> second))(value);
        } else {
            doPostponedPing();
        }
    }

    void onWait(const std::string& args) {
        doPostponedPing();
    }

    void onReady(const std::string& args) {
        doWrite("go \n");
    }

    void onStart(const std::string& names) {
        doPostponedPing();
    }

    void onMove(const std::string& args) {
        doWrite("state \n");
    }

    void onCurrent(const std::string& args) {
        doPostponedPing();
    };

    void doPostponedPing() {
        int millis = 500;
        timer.expires_from_now(boost::posix_time::millisec(millis));
        timer.async_wait(MEM_FN(doPing));
    }

    void doPing() {
        doWrite("ping \n");
    }

    void doRead() {
        async_read(mySocket, buffer(readBuffer),
                MEM_FN2(readComplete, _1, _2), MEM_FN2(onRead, _1, _2));
    }

    void doWrite(const std::string& msg) {
        if (!isStarted) {
            return;
        }
        std::copy(msg.begin(), msg.end(), writeBuffer);
        mySocket.async_write_some(buffer(writeBuffer, msg.size()), MEM_FN2(onWrite, _1, _2));
    }

    void start(ip::tcp::endpoint ep) {
        mySocket.async_connect(ep, MEM_FN1(onConnect, _1));
    }

    size_t readComplete(const boost::system::error_code& err, size_t bytes) {
        if(err) {
            return 0;
        }
        bool found = std::find(readBuffer, readBuffer + bytes, '\n') < readBuffer + bytes;
        // we read one-by-one until we get to enter, no buffering
        return found ? 0 : 1;
    }

    //    size_t readСomplete(const errorCode& err, size_t bytes) {
    //        if (err) return 0;
    //        bool found = std::find(readBuffer, readBuffer + bytes, '\n') < readBuffer + bytes;
    //        // we read one-by-one until we get to enter, no buffering
    //        return found ? 0 : 1;
    //    }

    typedef void(Client::*func)(const std::string&);
    typedef std::unordered_map<std::string, func> StringHandlerMap;
    ip::tcp::socket mySocket;

    enum {
        MAX_LENGTH = 4096
    };

    char readBuffer[MAX_LENGTH];
    char writeBuffer[MAX_LENGTH];

    bool isStarted;
    std::string username;
    deadline_timer timer;

    StringHandlerMap handlers;

};

