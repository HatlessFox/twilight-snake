#include <core/cellobject.hpp>

CellObject::CellObject(int x, int y, Type type) :
    m_x(x),
    m_y(y),
    m_type(type),
    m_snake_segment_type(CellObject::Unknown)
{
}

int CellObject::x() const
{
    return m_x;
}

int CellObject::y() const
{
    return m_y;
}

CellObject::Type CellObject::type() const
{
    return m_type;
}

CellObject::SnakeSegmentType CellObject::snake_segment_type() const
{
    return m_snake_segment_type;
}

void CellObject::set_type(CellObject::Type type)
{
    m_type = type;
}

void CellObject::set_snake_segment_type(CellObject::SnakeSegmentType type)
{
    m_snake_segment_type = type;
}
