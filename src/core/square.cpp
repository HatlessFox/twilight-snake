#include <core/square.hpp>
#include <vector>

Square::Square(size_t w, size_t h) :
    m_w(w),
    m_h(h),
    m_field(w, std::vector<std::shared_ptr<CellObject>>(h))
{
    for (size_t i = 0; i < w; ++i)
    {
        for (size_t j = 0; j < h; ++j)
        {
            m_field[i][j].reset(new CellObject(i, j));
        }
    }
}

std::shared_ptr<CellObject> Square::cell(int x, int y) const
{
    return m_field[x][y];
}

std::shared_ptr<CellObject> Square::next(std::shared_ptr<CellObject> cell, Direction dir) const
{
    int x = cell->x() + mx[dir];
    int y = cell->y() + my[dir];
    if (x < 0 || x >= m_w || y < 0 || y >= m_h)
    {
        return std::shared_ptr<CellObject>();
    }
    return m_field[x][y];
}
