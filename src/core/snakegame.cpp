#include <core/snakegame.hpp>
#include <boost/date_time.hpp>

SnakeGame::SnakeGame(size_t w, size_t h) :
    m_w(w),
    m_h(h),
    m_square( new Square(w, h) ),
    m_snake(new Snake( m_square->cell(w / 2, h / 2) )),
    m_is_game_over(false),
    m_distw(0, w-1),
    m_disth(0, h-1)
{
    generateFood();
    boost::posix_time::ptime time = boost::posix_time::second_clock::local_time();
    boost::posix_time::time_duration duration( time.time_of_day() );
    m_generator.seed( duration.total_nanoseconds() );
}

void SnakeGame::turn(Direction direction)
{
    m_snake->turn(direction);
}

void SnakeGame::step()
{
    if (m_is_game_over)
        return;

    std::cout << "step" << std::endl;
    std::shared_ptr<CellObject> cnext = m_square->next(m_snake->head(), m_snake->direction());
    if (!cnext)
    {
        m_is_game_over = true;
        return;
    }
    if (cnext->type() == CellObject::Food) {
        m_snake->move_head(cnext);
        generateFood();
    } else {
        m_snake->move_head(cnext);
        m_snake->move_tail();
    }
}

void SnakeGame::reset()
{
    m_square.reset( new Square(m_w, m_h) );
    m_snake.reset( new Snake(m_square->cell(m_w / 2, m_h / 2)) );
    m_is_game_over = false;
}

bool SnakeGame::isGameOver()
{
    return m_is_game_over;
}

void SnakeGame::generateFood() {
    std::cout << "generating food" << std::endl;
    bool isGenerated = false;
    while (!isGenerated) {
        size_t x = m_distw(m_generator);
        size_t y = m_disth(m_generator);
        if (m_square->cell(x, y)->type() == CellObject::Empty) {
            isGenerated = true;
            m_square->cell(x, y)->set_type(CellObject::Food);
        }
    }
}
