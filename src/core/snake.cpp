#include <core/snake.hpp>
#include <core/cellobject.hpp>

Snake::Snake(std::shared_ptr<CellObject> init) :
    m_direction(Direction::Up)
{
    m_cells.push_front(init);
    init->set_type(CellObject::Snake);
    init->set_snake_segment_type(CellObject::SnakeSegmentType::Vertical);
}

void Snake::move_head(std::shared_ptr<CellObject> head)
{
    int mx[4] = {0, 1, 0, -1};
    int my[4] = {1, 0, -1, 0};
    
    if (head->y() == m_cells.front()->y())
    {
        head->set_snake_segment_type(CellObject::SnakeSegmentType::Horizontal);
    }
    else
    {
        head->set_snake_segment_type(CellObject::SnakeSegmentType::Horizontal);
    }
    if (m_cells.front()->snake_segment_type() != head->snake_segment_type())
    {
        m_cells.front()->set_snake_segment_type(m_last_turn);
        m_last_turn = CellObject::SnakeSegmentType::Unknown;
    }
    m_cells.push_front(head);
    head->set_type(CellObject::Type::Snake);
}

void Snake::move_tail()
{
    m_cells.back()->set_type(CellObject::Empty);
    m_cells.back()->set_snake_segment_type(CellObject::SnakeSegmentType::Unknown);
    m_cells.pop_back();
}

std::shared_ptr<CellObject> Snake::head()
{
    return m_cells.front();
}

Direction Snake::direction()
{
    return m_direction;
}

void Snake::turn(Direction direction)
{
    if (m_direction == Direction::Up && direction == Direction::Down)
        return;
    if (m_direction == Direction::Down && direction == Direction::Up)
        return;
    if (m_direction == Direction::Left && direction == Direction::Right)
        return;
    if (m_direction == Direction::Right && direction == Direction::Left)
        return;
    
    Direction directions1[] = {Direction::Left, Direction::Up, Direction::Left, Direction::Down, Direction::Right, Direction::Up, Direction::Right, Direction::Down};
    Direction directions2[] = {Direction::Up, Direction::Left, Direction::Down, Direction::Left, Direction::Up, Direction::Right, Direction::Down, Direction::Right};
    CellObject::SnakeSegmentType res[] = {
        CellObject::SnakeSegmentType::LeftToUp, CellObject::SnakeSegmentType::LeftToUp,
        CellObject::SnakeSegmentType::LeftToDown, CellObject::SnakeSegmentType::LeftToDown,
        CellObject::SnakeSegmentType::RightToUp, CellObject::SnakeSegmentType::RightToUp,
        CellObject::SnakeSegmentType::RightToDown, CellObject::SnakeSegmentType::RightToDown
   };
    
    for (int i = 0; i < 8; ++i)
    {   
	    if (m_direction == directions1[i] && direction == directions2[i])
	    {
            m_last_turn = res[i];
	    }
    }
    m_direction = direction;   
}
