//
//  main.cpp
//
//  Created by Hatless Fox on 9/28/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#include <iostream>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <memory>
#include <functional>

#include "GameView.h"
#include "view/SnakeGameView.h"
#include "view/Program.h"
#include "view/Shaders.h"
#include "view/ObjFileLoader.h"
#include <GameView.h>
#include "view/SnakeGameView.h"

#include <core/snakegame.hpp>
#include <controller/gamecontroller.hpp>

#define MODEL_H 21
#define MODEL_W 27

int main(int argc, const char * argv[]) {
    // initialise GLFW
    if(!glfwInit())
        throw std::runtime_error("glfwInit failed");

    // open a window with GLFW
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    GLFWwindow* window = glfwCreateWindow(800, 600, "Twilight Snake", NULL, NULL);
    if (window == 0)
        throw std::runtime_error("window == NULL");

    glfwMakeContextCurrent(window);

    glewExperimental = GL_TRUE;
    if(glewInit() != GLEW_OK) { throw std::runtime_error("glewInit failed"); }
    // GLEW throws some errors, so discard all the errors so far
    while(glGetError() != GL_NO_ERROR) {}

    GameView* view = new SnakeGameView(window, MODEL_W, MODEL_H);
    std::auto_ptr<GameController> ctrl( GameController::instance(new SnakeGame(MODEL_W, MODEL_H), window, view) );
  
    ctrl->start();

    while (!glfwWindowShouldClose(window)) {
        view->begin();

        ctrl->update();

        ctrl->render();
        // check for errors
        GLenum error = glGetError();
        if(error != GL_NO_ERROR)
            std::cerr << "OpenGL Error " << error << ": " << (const char*)gluErrorString(error) << std::endl;

        view->commit();
//        glfwWaitEvents();
        glfwPollEvents();
    }
  
    // clean up and exit
    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}
