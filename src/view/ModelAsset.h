//
// Created by Vadim Lomshakov on 12/11/13.
// Copyright (c) 2013 WooHoo. All rights reserved.
//



#ifndef __ModelAsset_H_
#define __ModelAsset_H_

#include <GL/glew.h>
#include <vector>
#include "Program.h"

struct LightSourceData;
struct ModelAsset {
  std::shared_ptr<Program> shaders;
//  Texture* texture;
  GLuint vbo;
  GLuint vao;
  GLuint ibo;
  GLenum drawType;
  GLint drawCount;

  ModelAsset() :
//  texture(NULL),
  vbo(0),
  vao(0),
  ibo(0),
  drawType(GL_TRIANGLES),
  drawCount(0)
  {}

  virtual void drawAsset(glm::mat4 const &model, glm::mat4 const & proj,
      glm::mat4 const &camera, glm::vec3 const & color, std::vector<LightSourceData> const & lightSrcs) = 0;

  virtual ~ModelAsset() {};
};

#endif //__ModelAsset_H_
