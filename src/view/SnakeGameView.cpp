//
//  SnakeGameView.cpp
//  LightingDemo
//
//  Created by Vadim Lomshakov on 12/11/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#include "SnakeGameView.h"
#include "FieldAsset.h"
#import "SnakeAsset.h"

#include <glm/ext.hpp>


SnakeGameView::SnakeGameView(GLFWwindow* window, unsigned w, unsigned h):
  m_window(window),
  m_w(w), m_h(h),
  centerW(w / 2.0), centerH(h / 2.0),
  fieldAsset(new FieldAsset(m_h, m_w)),
  snakeAsset(new SnakeAsset())
{
  proj = glm::perspective(49.0f, 800.0f / 600.0f, .1f, 40.0f);
  camera = glm::lookAt(glm::vec3(centerH + 0.001, 25.0f, centerW), glm::vec3(centerH, 0, centerW), glm::vec3(0, 1, 0));

  // initialize light sources
  lightSrcs.push_back(LightSourceData(glm::vec3(centerH, 25.0, centerW), glm::vec3(1.0, 1.0, 1.0), glm::vec3(0, 1.0, 0), 0.0)); // .8
  //lightSrcs.push_back(LightSourceData(glm::vec3(centerH, 25.0, centerW), glm::vec3(.6, .6, .6), glm::vec3(-.25, 1.0, -.25), 0.97));
  //  lightSrcs.push_back(LightSourceData(glm::vec3(-1.0, 25.0, centerW), glm::vec3(.6, .6, .6), glm::vec3(-.25, 1.0, -.25), 0.97));

  
  // idx 0  is default light source
  assert(lightSrcs.size()  <= 5 && lightSrcs.size() > 0);

  glClearColor(0.0, 0.0, 0.0, 0.0);
  
  glEnable(GL_POLYGON_OFFSET_LINE);
  glEnable(GL_DEPTH_TEST);
}

inline float newLightPos() {
  return (300.0f - rand() % 600) / 1000;
}

void SnakeGameView::adjustLight(int i, float speed) {
  lightSrcs[i].spot_dir[0] += (rand() % 2 == 0 ? 1 : -1) * rand() % 100 * speed;
  lightSrcs[i].spot_dir[2] += (rand() % 2 == 0 ? 1 : -1) * rand() % 100 * speed;
  if (lightSrcs[i].spot_dir[0] > 0.45f) { lightSrcs[i].spot_dir[0] = newLightPos(); }
  if (lightSrcs[i].spot_dir[0] < -0.45f) { lightSrcs[i].spot_dir[0] = newLightPos(); }
  if (lightSrcs[i].spot_dir[2] > 0.45f) { lightSrcs[i].spot_dir[2] = newLightPos(); }
  if (lightSrcs[i].spot_dir[2] < -0.45f) { lightSrcs[i].spot_dir[2] = newLightPos(); }
}

void SnakeGameView::begin() {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  //  adjustLight(0, 0.0007);
  //adjustLight(1, 0.001);
  //adjustLight(2, 0.002);
  
  fieldAsset->drawAsset(glm::translate(glm::mat4(), glm::vec3((int)centerH, 0, (int)centerW)),
      proj, camera, glm::vec3(0.2 , 0.3, 0.2), lightSrcs);
}

void SnakeGameView::drawItem(unsigned hCell, unsigned vCell, CellType type) {

  glm::mat4 model = glm::translate(glm::mat4(), glm::vec3(hCell, 0, vCell));

  // debug Axises
/*  for (int i = 0; i != 27; ++i) {
    snakeAsset->drawAsset(glm::translate(glm::mat4(), glm::vec3((int)centerH, 0, i)), proj, camera, glm::vec3(0.0 , 0.2, 0.0), lightSrcs);
  }

  for (int i = 0; i != 21; ++i) {
    snakeAsset->drawAsset(glm::translate(glm::mat4(), glm::vec3(i, 0, (int)centerW)), proj, camera, glm::vec3(0.0 , 0.2, 0.0), lightSrcs);
  }*/

  glm::vec3 snake_color = glm::vec3(0.4, .4, .5);
  switch (type) {
    case FOOD:
      snakeAsset->drawAsset(model, proj, camera, glm::vec3(0.7 , 0, 0),lightSrcs);
      break;

    // parts of snake
    case SNAKE_HORIZ:
      snakeAsset->drawAsset(glm::rotate(model, 90.0f, glm::vec3(0, 1, 0)), proj, camera, snake_color, lightSrcs);
      break;
    case SNAKE_VERT:
      snakeAsset->drawAsset(glm::rotate(model, 0.0f, glm::vec3(0, 1, 0)), proj, camera, snake_color, lightSrcs);
      break;
    case SNAKE_LOW_LEFT:
      snakeAsset->drawAsset(glm::rotate(model, 135.0f, glm::vec3(0, 1, 0)), proj, camera, snake_color, lightSrcs);
      break;
    case SNAKE_LOW_RIGHT:
      snakeAsset->drawAsset(glm::rotate(model, - 135.0f, glm::vec3(0, 1, 0)), proj, camera, snake_color, lightSrcs);
      break;
    case SNAKE_UP_LEFT:
      snakeAsset->drawAsset(glm::rotate(model, 45.0f, glm::vec3(0, 1, 0)), proj, camera, snake_color, lightSrcs);
      break;
    case SNAKE_UP_RIGHT:
      snakeAsset->drawAsset(glm::rotate(model, - 45.0f, glm::vec3(0, 1, 0)), proj, camera, snake_color, lightSrcs);
      break;


    case EMPTY:
      break;
    default:
      throw std::logic_error("CellType arg is wrong in SnakeViewGame");
  }
}

void SnakeGameView::commit() {
  glfwSwapBuffers(m_window);
}
