//
//  SnakeGameView.h
//  LightingDemo
//
//  Created by Hatless Fox on 11/12/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#ifndef __LightingDemo__SnakeGameView__
#define __LightingDemo__SnakeGameView__

#include <iostream>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <memory>
#include <list>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "ModelAsset.h"
#include "Program.h"
#include "Shaders.h"
#include "ObjFileLoader.h"
#include "GameView.h"


struct LightSourceData {
  glm::vec3 position;
  glm::vec3 color;
  glm::vec3 spot_dir;
  float spot_cos_co;

  LightSourceData(const glm::vec3 &position, const glm::vec3 &color, const glm::vec3 &spot_dir, float spot_cos_co)
    : position(position), color(color), spot_dir(spot_dir), spot_cos_co(spot_cos_co)
  {}
};

class SnakeGameView : public GameView {
public:
  SnakeGameView(GLFWwindow* window, unsigned w = 0, unsigned h = 0);
  virtual void begin();
  virtual void drawItem(unsigned hCell, unsigned vCell, CellType type);
  virtual void commit();

private:
  void drawSnake(glm::mat4 const &model);
  void drawField(glm::mat4 const& model);
  void loadBunnyAsset();
  void loadFieldAsset(int h, int w);

  void adjustLight(int index, float speed);
private:
  unsigned m_w;
  unsigned m_h;

  float centerW;
  float centerH;

  glm::mat4 proj;
  glm::mat4 camera;

  GLFWwindow* m_window;
  std::shared_ptr<ModelAsset> fieldAsset;
  std::shared_ptr<ModelAsset> snakeAsset;

  std::vector<LightSourceData> lightSrcs;
};

#endif /* defined(__LightingDemo__SnakeGameView__) */
