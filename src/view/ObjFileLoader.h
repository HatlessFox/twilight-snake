//
//  ObjFileLoader.h
//  test_Graphics
//
//  Created by Hatless Fox on 10/12/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#ifndef __test_Graphics__ObjFileLoader__
#define __test_Graphics__ObjFileLoader__

#include <iostream>
#include <string>
#include <vector>
#include <sstream>

class ObjFileLoader {
public: // methods
	void loadFromFile(std::string const & file_name);
	float *loaded_vtx_coords();
	size_t loaded_vtx_coords_size();

  float *loaded_vtx_normals();
	size_t loaded_vtx_normals_size();
  
	int *loaded_faces();
	size_t loaded_faces_size();
	size_t elems_cnt();
private: // methods
	void handle_vtx(std::stringstream &data);
	void handle_face(std::stringstream &data);
	void handle_nvtx(std::stringstream &data);
private: // fields
	std::vector<float> vtx_coord_data;
  std::vector<float> vtx_normal_data;
	std::vector<int> faces_data; // faces defined with indeces
};

#endif /* defined(__test_Graphics__ObjFileLoader__) */
