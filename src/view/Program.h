//
//  Program.h
//  LightingDemo
//
//  Created by Hatless Fox on 11/11/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#ifndef LightingDemo_Program_h
#define LightingDemo_Program_h

#include <GL/glew.h>
#include <iostream>
#include <string>

#include "Shader.h"

class Program {
public: // methods
  Program(const char * vtx_sh, const char * frag_sh) {
    m_prog_id = glCreateProgram();
    if (m_prog_id == 0) { throw std::runtime_error("Error creating shader program\n"); }
    
    Shader vtx(vtx_sh, GL_VERTEX_SHADER);
    Shader frg(frag_sh, GL_FRAGMENT_SHADER);
    
    glAttachShader(m_prog_id, vtx.descr());
    glAttachShader(m_prog_id, frg.descr());
    
    glLinkProgram(m_prog_id);
    checkProgramErrors(m_prog_id, GL_LINK_STATUS);
  }
  
  inline void use() { glUseProgram(m_prog_id); }
  inline void stopUsing() { glUseProgram(0); }
  
  ~Program() {
    if (m_prog_id != 0) { glDeleteProgram(m_prog_id); }
  }
  
  inline GLuint desc() { return m_prog_id; }
  
  
  GLint attLc(const GLchar* attribute_name) const {
    GLint attrib_loc = glGetAttribLocation(m_prog_id, attribute_name);
    if(attrib_loc == -1) {
      throw std::runtime_error(std::string("Program attribute not found: ") +
                               attribute_name);
    }
    return attrib_loc;
  }
  
  GLint uniLc(const GLchar* uniform_name) const {
    GLint uniform_loc = glGetUniformLocation(m_prog_id, uniform_name);
    if(uniform_loc == -1) {
      throw std::runtime_error(std::string("Program uniform not found: ") +
                               uniform_name);
    }
    return uniform_loc;
  }
  
private: // methods
  void checkProgramErrors(GLint shaderProgram, GLenum checkParam) {
    GLint success = 0;
    glGetProgramiv(shaderProgram, checkParam, &success);
    if (!success) {
      GLchar log[1024];
      glGetProgramInfoLog(shaderProgram, sizeof(log), NULL, log);
      std::cerr << log << std::endl;
      
      glDeleteProgram(m_prog_id);
      throw std::runtime_error("Program check error");
    }
  }
  
private: // field
  GLuint m_prog_id;
};

#endif
