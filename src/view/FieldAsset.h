//
// Created by Vadim Lomshakov on 12/11/13.
// Copyright (c) 2013 WooHoo. All rights reserved.
//



#include "ModelAsset.h"
#include "glm/core/type.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "Shaders.h"
#include <string>

#ifndef __FieldAsset_H_
#define __FieldAsset_H_


struct FieldAsset: ModelAsset {

private:
  unsigned _h;
  unsigned  _w;
public:

  FieldAsset(unsigned h, unsigned w): _h(h), _w(w) {
    loadAsset();
  }

  void drawAsset(glm::mat4 const& model, glm::mat4 const & proj, glm::mat4 const &camera, glm::vec3 const & color, std::vector<LightSourceData> const & lightSrcs) {
    glDepthFunc(GL_LESS);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    shaders->use();

    glBindVertexArray(vao);

//  glActiveTexture(GL_TEXTURE0);
//  glBindTexture(GL_TEXTURE_2D, >texture->object());

    glUniformMatrix4fv(shaders->uniLc("worldMatrix"), 1.0f, GL_FALSE, glm::value_ptr(proj * camera));
    glUniformMatrix4fv(shaders->uniLc("model_matr"), 1.0f, GL_FALSE, glm::value_ptr(model));
    glUniform3fv(shaders->uniLc("color"), 1, glm::value_ptr(color));

    glUniform1i(shaders->uniLc("lights_cnt"), lightSrcs.size());

    for (int i = 0; i != lightSrcs.size(); ++i) {
      std::string idx_str = std::to_string(i);
      glUniform3fv(shaders->uniLc(("all_lights[" + idx_str +  "].position").c_str()), 1, glm::value_ptr(lightSrcs[i].position));
      glUniform3fv(shaders->uniLc(("all_lights[" + idx_str + "].color").c_str()), 1, glm::value_ptr(lightSrcs[i].color));
      glUniform3fv(shaders->uniLc(("all_lights[" + idx_str + "].spot_dir").c_str()), 1, glm::value_ptr(lightSrcs[i].spot_dir));
      glUniform1f(shaders->uniLc(("all_lights[" + idx_str + "].spot_cos_co").c_str()), lightSrcs[i].spot_cos_co); //0 for diffuse light use .9 or so
    }
    glDrawArrays(drawType, 0, drawCount);


    glBindVertexArray(0);
//  glBindTexture(GL_TEXTURE_2D, 0)
    shaders->stopUsing();
  }

private:

  void loadAsset() {
    float hh = _h / 2.0;
    float ww = _w / 2.0;

    shaders.reset(new Program(plain_colored_vtx_sh, plain_colored_frg_sh));
    drawType = GL_TRIANGLES;
    //  cow.texture = LoadTexture(".jpg");

    // orientation is CCW
    GLfloat vertexData[] = {
    //    X    Y    Z        NX    NY    NZ
        -hh,.0f, ww,    0.0f, 1.0f, 0.0f,
        hh, .0f, ww,    0.0f, 1.0f, 0.0f,
        hh,.0f, -ww,    0.0f, 1.0f, 0.0f,

        hh, .0f, -ww,    0.0f, 1.0f, 0.0f,
        -hh,.0f, -ww,    0.0f, 1.0f, 0.0f,
        -hh, .0f, ww,    0.0f, 1.0f, 0.0f,
    };


    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), vertexData, GL_STATIC_DRAW);


    glEnableVertexAttribArray(shaders->attLc("position"));
    glEnableVertexAttribArray(shaders->attLc("normal"));


    GLsizei stride = 6 * sizeof(GLfloat);
    glVertexAttribPointer(shaders->attLc("position"), 3, GL_FLOAT, GL_FALSE, stride, NULL);
    glVertexAttribPointer(shaders->attLc("normal"), 3, GL_FLOAT, GL_TRUE, stride, (const GLvoid *)(3*sizeof(GLfloat)));


    glBindVertexArray(0);

    drawCount =  2*3;
  }
};


#endif //__FieldAsset_H_
