//
// Created by Vadim Lomshakov on 12/11/13.
// Copyright (c) 2013 WooHoo. All rights reserved.
//



#include "ModelAsset.h"
#include "Shaders.h"
#include "ObjFileLoader.h"
#include "glm/core/type.hpp"
#include "glm/gtc/type_ptr.hpp"

#ifndef __SnakeAsset_H_
#define __SnakeAsset_H_


struct SnakeAsset: ModelAsset {

  SnakeAsset() {
    loadAsset();
  }

  void drawAsset(glm::mat4 const &model, glm::mat4 const & proj, glm::mat4 const &camera, glm::vec3 const & color, std::vector<LightSourceData> const & lightSrcs) {
    glDepthFunc(GL_LESS);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    shaders->use();

    glBindVertexArray(vao);

//  glActiveTexture(GL_TEXTURE0);
//  glBindTexture(GL_TEXTURE_2D, >texture->object());

    glUniformMatrix4fv(shaders->uniLc("worldMatrix"), 1.0f, GL_FALSE, glm::value_ptr(proj * camera));
    glUniformMatrix4fv(shaders->uniLc("model_matr"), 1.0f, GL_FALSE, glm::value_ptr(model));
    glUniform3fv(shaders->uniLc("color"), 1, glm::value_ptr(color));

    glUniform1i(shaders->uniLc("lights_cnt"), lightSrcs.size());

    for (int i = 0; i != lightSrcs.size(); ++i) {
      std::string idx_str = std::to_string(i);
      glUniform3fv(shaders->uniLc(("all_lights[" + idx_str +  "].position").c_str()), 1, glm::value_ptr(lightSrcs[i].position));
      glUniform3fv(shaders->uniLc(("all_lights[" + idx_str + "].color").c_str()), 1, glm::value_ptr(lightSrcs[i].color));
      glUniform3fv(shaders->uniLc(("all_lights[" + idx_str + "].spot_dir").c_str()), 1, glm::value_ptr(lightSrcs[i].spot_dir));
      glUniform1f(shaders->uniLc(("all_lights[" + idx_str + "].spot_cos_co").c_str()), lightSrcs[i].spot_cos_co);
    }

    glDrawElements(drawType, drawCount, GL_UNSIGNED_INT, 0);


    glBindVertexArray(0);
//  glBindTexture(GL_TEXTURE_2D, 0)
    shaders->stopUsing();
  }

private:

  void loadAsset() {
    shaders.reset(new Program(plain_colored_vtx_sh, plain_colored_frg_sh));
    drawType = GL_TRIANGLES;
    //  cow.texture = LoadTexture(".jpg");

    ObjFileLoader ldr;
    // TODO get resource function
    ldr.loadFromFile("./data/bunny_n.obj");//"./data/bunny_n.obj")

    //merge coords with normals
    assert(ldr.loaded_vtx_coords_size() == ldr.loaded_vtx_normals_size());

    std::vector<float> merged_data;
    float *coords = ldr.loaded_vtx_coords();
    float *normarls = ldr.loaded_vtx_normals();
    for (size_t i = 0; i < ldr.loaded_vtx_coords_size() / sizeof(float); i += 3) {
      for (size_t j = 0; j < 3; ++j) { merged_data.push_back(10 * (*coords++)); }
      for (size_t j = 0; j < 3; ++j) { merged_data.push_back(*normarls++); }
    }

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);


    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * merged_data.size(),
        &merged_data[0], GL_STATIC_DRAW);

    glGenBuffers(1, &ibo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, ldr.loaded_faces_size(),
        ldr.loaded_faces(), GL_STATIC_DRAW);


    glEnableVertexAttribArray(shaders->attLc("position"));
    glEnableVertexAttribArray(shaders->attLc("normal"));


    GLsizei stride = 6 * sizeof(GLfloat);
    glVertexAttribPointer(shaders->attLc("position"), 3, GL_FLOAT, GL_FALSE, stride, NULL);
    glVertexAttribPointer(shaders->attLc("normal"), 3, GL_FLOAT, GL_TRUE, stride, (const GLvoid *)(3*sizeof(GLfloat)));


    glBindVertexArray(0);

    drawCount = (GLint)ldr.elems_cnt();
  }

};


#endif //__SnakeAsset_H_
