//
//  Shaders.h
//  test_Graphics
//
//  Created by Hatless Fox on 10/15/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#ifndef test_Graphics_Shaders_h
#define test_Graphics_Shaders_h

#include "Shader.h"

/**********************/

static const char* plain_colored_vtx_sh = "\
  #version 150 \n\
  in vec3 position; \
  in vec3 normal;  \
  \
  out vec3 vtx_pos; \
  out vec3 vtx_normal; \
  \
  uniform mat4 model_matr; \
  uniform mat4 worldMatrix; \
  \
  void main() { \
    vtx_pos = position; \
    vtx_normal = normal; \
  \
  gl_Position = worldMatrix * model_matr * vec4(position, 1.0); \
}";


static const char* plain_colored_frg_sh = "\
  #version 150 \n\
  in vec3 vtx_pos; \
  in vec3 vtx_normal; \
  \
  uniform mat4 model_matr; \
  uniform vec3 color;  \
  \
  uniform int lights_cnt; \
  uniform struct Light { \
    vec3 position; \
    vec3 color; \
    vec3 spot_dir; \
    float spot_cos_co; \
  } all_lights[5];\
  \
  out vec4 frag_color;\
  \
  void main() {\
    mat3 normal_mtr = transpose(inverse(mat3(model_matr))); \
    vec3 normal = normalize(normal_mtr * vtx_normal); \
    \
    vec3 pos = vec3(model_matr * vec4(vtx_pos, 1)); \
    \
    frag_color = vec4(0.0);\
    \
    for (int i = 0; i < lights_cnt; ++i) {\
      vec3 light_dir = all_lights[i].position - pos; \
      \
      vec3 n_LD = normalize(light_dir);\
      vec3 n_SD = normalize(all_lights[i].spot_dir);\
      float brightness = 0.03;\
      float dp = dot(n_LD, n_SD);\
      if (dp > all_lights[i].spot_cos_co) {\
        float coeff = mix(0, 1.0, (dp - all_lights[i].spot_cos_co) / (1.0 - all_lights[i].spot_cos_co));\
        brightness = coeff * clamp(dot(normal, n_LD), 0, 1); \
      }\
      frag_color += brightness * vec4(all_lights[i].color, 1) * vec4(color, 1); \
    }\
  }";

////////////////////////////////////////////////////////////////////////////////

#endif
